module CsSyd.Offerter where

import CsSyd.Offerter.Automate
import CsSyd.Offerter.Generate
import CsSyd.Offerter.OptParse

offerter :: IO ()
offerter = do
  (disp, Settings) <- getInstructions
  dispatch disp

dispatch :: Dispatch -> IO ()
dispatch DispatchAutomate = automate
dispatch (DispatchGenerate ga) = generate ga
