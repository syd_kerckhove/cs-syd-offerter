{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module CsSyd.Offerter.Automate where

import Control.Monad
import CsSyd.Docter
import CsSyd.Offerter.Types
import Data.Maybe
import Path
import Path.IO
import System.Exit
import Text.Mustache

automate :: IO ()
automate = do
  here <- getCurrentDir
  let rawDir = here </> rawDataDir
      autoDir = here </> autoGenDir
      manualDir = here </> manualDataDir
  fs <- fromMaybe [] <$> forgivingAbsence (snd <$> listDirRecur rawDir)
  template <-
    do
      errOrTempl <-
        automaticCompile
          [toFilePath manualDir]
          (toFilePath $ manualDir </> mainTemplateFile)
      case errOrTempl of
        Left err ->
          die $ unwords ["Failed to compile quote template", show err]
        Right tmpl -> pure tmpl
  forM_ (filter (not . hidden) fs) $ \f -> do
    i <- readJSONOrYaml f
    case stripProperPrefix rawDir f of
      Nothing -> pure ()
      Just rf -> genPdfFor template (autoDir </> rf) (i :: Quote)

quotesPathFromRoot :: Path Rel Dir
quotesPathFromRoot = $(mkRelDir "data/quotes")

rawDataDir :: Path Rel Dir
rawDataDir = $(mkRelDir "raw")

autoGenDir :: Path Rel Dir
autoGenDir = $(mkRelDir "auto")

manualDataDir :: Path Rel Dir
manualDataDir = $(mkRelDir "manual")

mainTemplateFile :: Path Rel File
mainTemplateFile = $(mkRelFile "quote.tex")
