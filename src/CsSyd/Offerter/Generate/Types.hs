module CsSyd.Offerter.Generate.Types where

import CsSyd.Docter
import CsSyd.Offerter.Types
import qualified Data.Time as Time

data GenerationAssignment
  = GenerationAssignment
      { genOutputDirective :: OutputDirective,
        genQuoteFrom :: QuoteFrom,
        genQuoteTo :: QuoteTo,
        genDate :: Time.Day,
        genExpiryDate :: Maybe Time.Day
      }
  deriving (Show, Eq)
