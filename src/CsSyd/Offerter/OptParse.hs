{-# LANGUAGE RecordWildCards #-}

module CsSyd.Offerter.OptParse
  ( module CsSyd.Offerter.OptParse,
    module CsSyd.Offerter.OptParse.Types,
  )
where

import CsSyd.Offerter.OptParse.Types
import Data.Maybe
import qualified Data.Time as Time
import Options.Applicative
import Path
import Path.IO
import System.Environment as System

getInstructions :: IO Instructions
getInstructions = do
  (cmd, flags) <- getArguments
  config <- getConfiguration cmd flags
  combineToInstructions cmd flags config

combineToInstructions :: Command -> Flags -> Configuration -> IO Instructions
combineToInstructions cmd_ Flags Configuration = (,) <$> disp <*> pure Settings
  where
    disp =
      case cmd_ of
        CommandAutomate -> pure DispatchAutomate
        CommandGenerate fs -> DispatchGenerate <$> generationSettings fs

generationSettings :: GenerationFlags -> IO GenerationSettings
generationSettings GenerationFlags {..} =
  GenerationSettings
    <$> ( case generateOutput of
            Nothing -> pure Nothing
            Just fp -> Just <$> resolveFile' fp
        )
    <*> parseRel generateFrom
    <*> parseRel generateTo
    <*> ( case generateDay of
            Nothing -> pure Nothing
            Just d -> Just <$> Time.parseTimeM True Time.defaultTimeLocale "%F" d
        )
    <*> pure generateExpiry
  where
    parseRel :: Maybe FilePath -> IO (Maybe (Path Rel File))
    parseRel Nothing = pure Nothing
    parseRel (Just f) = Just <$> parseRelFile f

getConfiguration :: Command -> Flags -> IO Configuration
getConfiguration _ _ = pure Configuration

getArguments :: IO Arguments
getArguments = do
  args <- System.getArgs
  let result = runArgumentsParser args
  handleParseResult result

runArgumentsParser :: [String] -> ParserResult Arguments
runArgumentsParser = execParserPure prefs_ argParser
  where
    prefs_ =
      ParserPrefs
        { prefMultiSuffix = "",
          prefDisambiguate = True,
          prefShowHelpOnError = True,
          prefShowHelpOnEmpty = True,
          prefBacktrack = True,
          prefColumns = 80
        }

argParser :: ParserInfo Arguments
argParser = info (helper <*> parseArgs) help_
  where
    help_ = fullDesc <> progDesc description
    description = "Offerter"

parseArgs :: Parser Arguments
parseArgs = (,) <$> parseCommand <*> parseFlags

parseCommand :: Parser Command
parseCommand =
  hsubparser $
    mconcat
      [ command "automate" parseCommandAutomate,
        command "generate" parseCommandGenerate
      ]

parseCommandAutomate :: ParserInfo Command
parseCommandAutomate = info parser modifier
  where
    parser = pure CommandAutomate
    modifier = fullDesc <> progDesc "Automate quotes"

parseCommandGenerate :: ParserInfo Command
parseCommandGenerate = info parser modifier
  where
    parser = CommandGenerate <$> parseGenerateFlags
    modifier = fullDesc <> progDesc "Generate a new quote"
    parseGenerateFlags =
      GenerationFlags
        <$> option
          (Just <$> str)
          ( mconcat
              [ long "output",
                short 'o',
                metavar "FILE",
                help "Where to output the result to",
                showDefaultWith $ fromMaybe "Standard output",
                value Nothing
              ]
          )
        <*> option
          (Just <$> str)
          ( mconcat
              [ long "from",
                metavar "FILENAME",
                help "The 'from' to use, specified as a filename.",
                showDefaultWith $ fromMaybe "Empty InvoiceFrom",
                value Nothing
              ]
          )
        <*> option
          (Just <$> str)
          ( mconcat
              [ long "to",
                metavar "FILENAME",
                help "The 'to' to use, specified as a filename.",
                showDefaultWith $ fromMaybe "Empty InvoiceTo",
                value Nothing
              ]
          )
        <*> option
          (Just <$> str)
          ( mconcat
              [ long "date",
                metavar "DATE",
                help "The quote date",
                showDefaultWith $ fromMaybe "Today",
                value Nothing
              ]
          )
        <*> option
          (Just <$> auto)
          ( mconcat
              [ long "expiry-days",
                metavar "NUMBER",
                help
                  "The number of days between the quote date and the expiry date",
                showDefaultWith $ maybe "Empty expiry date" show,
                value Nothing
              ]
          )

parseFlags :: Parser Flags
parseFlags = pure Flags
