{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module CsSyd.Offerter.Generate where

import Control.Applicative
import CsSyd.Docter
import CsSyd.Offerter.Generate.Types
import CsSyd.Offerter.OptParse.Types
import CsSyd.Offerter.Types
import Data.Aeson
import qualified Data.ByteString.Char8 as SB8
import qualified Data.Time as Time
import qualified Data.Yaml as Yaml
import Path
import Path.IO
import System.Exit

generate :: GenerationSettings -> IO ()
generate sets = do
  GenerationAssignment {..} <- assignGeneration sets
  let invoice =
        emptyQuote
          { quoteFrom = genQuoteFrom,
            quoteTo = genQuoteTo,
            quoteDate = genDate,
            quoteExpiryDate = genExpiryDate
          }
  case genOutputDirective of
    ToStdOut -> SB8.putStrLn $ Yaml.encode invoice
    ToFile f -> writeYaml f invoice

assignGeneration :: GenerationSettings -> IO GenerationAssignment
assignGeneration GenerationSettings {..} =
  GenerationAssignment outputd <$> getfrom <*> getto <*> getdate
    <*> getduedate
  where
    outputd :: OutputDirective
    outputd =
      case generateSetsOutput of
        Nothing -> ToStdOut
        Just f -> ToFile f
    getfrom :: IO QuoteFrom
    getfrom =
      getJSONOrYAMLWith
        generateSetsFrom
        emptyQuoteFrom
        ($(mkRelDir "from/") </>)
    getto :: IO QuoteTo
    getto =
      getJSONOrYAMLWith generateSetsTo emptyQuoteTo ($(mkRelDir "to/") </>)
    getJSONOrYAMLWith ::
      FromJSON a =>
      Maybe (Path Rel File) ->
      a ->
      (Path Rel File -> Path Rel File) ->
      IO a
    getJSONOrYAMLWith mf def_ func =
      case mf of
        Nothing -> pure def_
        Just tof -> do
          jpn <- setFileExtension ".json" $ func tof
          ypn <- setFileExtension ".yaml" $ func tof
          here <- getCurrentDir
          mjaf <- searchFile here jpn
          myaf <- searchFile here ypn
          case mjaf <|> myaf of
            Nothing ->
              die $
                unlines
                  [ "Files",
                    toFilePath jpn,
                    "and",
                    toFilePath ypn,
                    "not found anywhere under",
                    toFilePath here
                  ]
            Just af -> readJSONOrYaml af
    getdate :: IO Time.Day
    getdate =
      case generateSetsDay of
        Nothing -> do
          now <- Time.getZonedTime
          let nowutc = Time.zonedTimeToUTC now
          pure $ Time.utctDay nowutc
        Just d -> pure d
    getduedate =
      case generateSetsExpiry of
        Nothing -> pure Nothing
        Just i -> (Just . Time.addDays i) <$> getdate
