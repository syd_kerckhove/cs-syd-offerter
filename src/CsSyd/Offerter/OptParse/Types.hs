module CsSyd.Offerter.OptParse.Types where

import qualified Data.Time as Time
import Path

type Arguments = (Command, Flags)

type Instructions = (Dispatch, Settings)

data Command
  = CommandAutomate
  | CommandGenerate GenerationFlags
  deriving (Show, Eq)

data Flags
  = Flags
  deriving (Show, Eq)

data Configuration
  = Configuration
  deriving (Show, Eq)

data Dispatch
  = DispatchAutomate
  | DispatchGenerate GenerationSettings
  deriving (Show, Eq)

data Settings
  = Settings
  deriving (Show, Eq)

data GenerationFlags
  = GenerationFlags
      { generateOutput :: Maybe FilePath,
        generateFrom :: Maybe FilePath,
        generateTo :: Maybe FilePath,
        generateDay :: Maybe String,
        generateExpiry :: Maybe Integer
      }
  deriving (Show, Eq)

data GenerationSettings
  = GenerationSettings
      { generateSetsOutput :: Maybe (Path Abs File),
        generateSetsFrom :: Maybe (Path Rel File),
        generateSetsTo :: Maybe (Path Rel File),
        generateSetsDay :: Maybe Time.Day,
        generateSetsExpiry :: Maybe Integer
      }
  deriving (Show, Eq)
