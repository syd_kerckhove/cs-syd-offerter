{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module CsSyd.Offerter.Types where

import CsSyd.Docter
import Data.Aeson
import Data.Maybe
import Data.Text (Text)
import qualified Data.Text as T
import Data.Time as Time
import Data.Validity
import Data.Validity.Text ()
import Data.Validity.Time ()
import GHC.Generics (Generic)
import Text.Pandoc

data Quote
  = Quote
      { quoteFrom :: QuoteFrom,
        quoteTo :: QuoteTo,
        quoteDate :: Day,
        quoteExpiryDate :: Maybe Day,
        quoteDescription :: Text, -- Markdown
        quoteCosts :: [QuoteCost]
      }
  deriving (Show, Eq, Generic)

instance Validity Quote

instance FromJSON Quote where
  parseJSON =
    withObject "Quote" $ \o ->
      Quote <$> o .: "from" <*> o .: "to" <*> o .: "date"
        <*> o .: "expiry date"
        <*> o .: "description"
        <*> (fromMaybe [] <$> o .:? "costs")

instance ToJSON Quote where
  toJSON Quote {..} =
    object
      [ "from" .= quoteFrom,
        "to" .= quoteTo,
        "date" .= quoteDate,
        "expiry date" .= quoteExpiryDate,
        "description" .= quoteDescription,
        "costs" .= quoteCosts
      ]

instance ToJSON (ForMustache Quote) where
  toJSON (ForMustache Quote {..}) =
    object
      [ "from" .= ForMustache quoteFrom,
        "to" .= ForMustache quoteTo,
        "date" .= ForMustache quoteDate,
        "expiry-date" .= ForMustache quoteExpiryDate,
        "description" .= markdownFor quoteDescription,
        "costs" .= map ForMustache quoteCosts,
        "total"
          .= object
            [ "expected"
                .= ForMustache (priceAdds $ map costExpected quoteCosts),
              "maximum"
                .= ForMustache
                  ( do
                      ps <- mapM costMaximum quoteCosts
                      pure $ priceAdds ps
                  )
            ]
      ]

emptyQuote :: Quote
emptyQuote =
  Quote
    { quoteFrom = emptyQuoteFrom,
      quoteTo = emptyQuoteTo,
      quoteDate = Time.fromGregorian 1970 1 1,
      quoteExpiryDate = Nothing,
      quoteDescription = mempty,
      quoteCosts = []
    }

data QuoteFrom
  = QuoteFrom
      { fromAddress :: Address,
        fromPhone :: PhoneNumber,
        fromEmail :: EmailAddress
      }
  deriving (Show, Eq, Generic)

instance Validity QuoteFrom

instance FromJSON QuoteFrom where
  parseJSON =
    withObject "QuoteFrom" $ \o ->
      QuoteFrom <$> o .: "address" <*> o .: "phone" <*> o .: "email"

instance ToJSON QuoteFrom where
  toJSON QuoteFrom {..} =
    object
      [ "address" .= fromAddress,
        "phone" .= fromPhone,
        "email" .= fromEmail
      ]

instance ToJSON (ForMustache QuoteFrom) where
  toJSON (ForMustache QuoteFrom {..}) =
    object
      [ "address" .= ForMustache fromAddress,
        "phone" .= ForMustache fromPhone,
        "email" .= fromEmail
      ]

emptyQuoteFrom :: QuoteFrom
emptyQuoteFrom =
  QuoteFrom
    { fromAddress = emptyAddress,
      fromPhone = emptyPhoneNumber,
      fromEmail = emptyEmailAddress
    }

newtype QuoteTo
  = QuoteTo
      { toAddress :: Address
      }
  deriving (Show, Eq, Generic)

instance Validity QuoteTo

instance FromJSON QuoteTo where
  parseJSON = withObject "QuoteTo" $ \o -> QuoteTo <$> o .: "address"

instance ToJSON QuoteTo where
  toJSON QuoteTo {..} = object ["address" .= toAddress]

instance ToJSON (ForMustache QuoteTo) where
  toJSON (ForMustache QuoteTo {..}) =
    object ["address" .= ForMustache toAddress]

emptyQuoteTo :: QuoteTo
emptyQuoteTo = QuoteTo {toAddress = emptyAddress}

data QuoteCost
  = QuoteCost
      { costDescription :: Text, -- Markdown
        costExpected :: Price,
        costMaximum :: Maybe Price
      }
  deriving (Show, Eq, Generic)

instance Validity QuoteCost

instance FromJSON QuoteCost where
  parseJSON =
    withObject "QuoteCost" $ \o ->
      QuoteCost <$> o .: "description" <*> o .: "expected"
        <*> o .: "maximum"

instance ToJSON QuoteCost where
  toJSON QuoteCost {..} =
    object
      [ "description" .= costDescription,
        "expected" .= costExpected,
        "maximum" .= costMaximum
      ]

instance ToJSON (ForMustache QuoteCost) where
  toJSON (ForMustache QuoteCost {..}) =
    object
      [ "description" .= markdownFor costDescription,
        "expected" .= ForMustache costExpected,
        "maximum" .= (ForMustache <$> costMaximum)
      ]

markdownFor :: Text -> Value
markdownFor t =
  object
    [ "raw" .= t,
      "rendered"
        .= ( either (T.pack . show) id
               $ runPure
               $ ((writeLaTeX def =<< (readMarkdown def t :: PandocPure Pandoc)) :: PandocPure Text)
           )
    ]
