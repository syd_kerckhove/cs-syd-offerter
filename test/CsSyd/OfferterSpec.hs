{-# LANGUAGE TypeApplications #-}

module CsSyd.OfferterSpec
  ( spec,
  )
where

import CsSyd.Offerter.Gen ()
import CsSyd.Offerter.Types
import Test.Hspec
import Test.Validity
import Test.Validity.Aeson

spec :: Spec
spec = do
  genValidSpec @Quote
  jsonSpecOnValid @Quote
  genValidSpec @QuoteFrom
  jsonSpecOnValid @QuoteFrom
  genValidSpec @QuoteTo
  jsonSpecOnValid @QuoteTo
  genValidSpec @QuoteCost
  jsonSpecOnValid @QuoteCost
