{-# OPTIONS_GHC -Wno-orphans #-}

module CsSyd.Offerter.Gen where

import CsSyd.Docter.Gen ()
import CsSyd.Offerter.Types
import Data.GenValidity
import Data.GenValidity.Time ()

instance GenValid Quote where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally

instance GenValid QuoteFrom where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally

instance GenValid QuoteTo where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally

instance GenValid QuoteCost where
  genValid = genValidStructurally
  shrinkValid = shrinkValidStructurally
