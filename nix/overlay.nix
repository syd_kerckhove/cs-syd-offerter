final: previous:
with final.haskell.lib;

let
  offerter =
          doBenchmark (
              failOnAllWarnings (
                disableLibraryProfiling (
                  final.haskellPackages.callCabal2nix "offerter" ( final.gitignoreSource ( ../. ) ) {}
                )
            )
            );
in
{
  inherit offerter;
  haskellPackages =
    previous.haskellPackages.override (
      old:
        {
          overrides =
            final.lib.composeExtensions (
              old.overrides or (_:
            _:
              {})
            ) (
              self: super:
                let
                  docterRepo =
                    final.fetchgit {
                      url = "https://bitbucket.org/syd_kerckhove/cs-syd-docter";
                      rev = "2f83f7586fae37a7374b77db91d04c59ad02538b";
                      sha256 =
                        "sha256:1a7h5vhkn4c8rg1jzgyx30kadwh6g619ryn3rimsp1lr2rji5pxn";
                    };

                  docterPkg =
                    name:
                      disableLibraryProfiling (
                          self.callCabal2nix name ( docterRepo + "/${name}" ) {}
                      );
                  docterPackages =
                    final.lib.genAttrs [
                      "docter"
                      "docter-data-gen"
                    ] docterPkg;
                in
                docterPackages // {
                  "offerter" = offerter;
              } 
            );
        }
    );
}
